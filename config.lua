local settings = minetest.settings
local config = {
	logs = {},
	flint = {},
	fibre = {}
}

local function configList(group, name, default)
	local list = {}
	for _, v in pairs(realtest.util.split(settings:get(group.."."..name) or default or "", "%s*,%s*")) do
		list[v] = true
	end
	config[group][name] = list
end

local function configLists(group, name)
	configList(group, name.."_blacklist")
	configList(group, name.."_whitelist")
end

local function clamp(name, default, min, max)
	return realtest.util.clamp(
			math.floor(
				tonumber(
					settings:get(name) or default or 1)),
		min or 1, max or 99)
end

-- Deserialize the settings
config = {
	logs = {
		enable_chopping = settings:get_bool("logs.enable_chopping", true),
		log_plank_count = clamp("logs.log_plank_count", 4),
		stem_plank_count = clamp("logs.stem_plank_count", 1),
		enable_auto_axe_whitelist = settings:get_bool("logs.enable_auto_axe_whitelist", true),
		enable_auto_tree_whitelist = settings:get_bool("logs.enable_auto_tree_whitelist", true)
	},
	flint = {
		enable_flint_shards = settings:get_bool("flint.enable_flint_shards", true),
		enable_auto_stone_whitelist = settings:get_bool("flint.enable_auto_stone_whitelist", true),
		flint_shard_count = clamp("flint.flint_shards_count", 2),
		enable_flint_tools = settings:get_bool("flint.enable_flint_tools", true)
	},
	fibre = {
		plant_fibre_drop_chance = clamp("fibre.plant_fibre_drop_chance", 5, 1, 10000),
		plant_fibre_count = clamp("fibre.plant_fibre_count", 1),
		enable_auto_grass_whitelist = settings:get_bool("fibre.enable_auto_grass_whitelist", true)
	},
	tweaks = {
		leaf_stick_drop_chance = clamp("tweaks.leaf_stick_drop_chance", 5, 0, 10000),
		fast_gravel_mining = settings:get_bool("tweaks.fast_gravel_mining", false),
		enable_lumps = settings:get_bool("tweaks.enable_lumps", true)
	}
}

configLists("logs", "axe")
configLists("logs", "tree")
configLists("flint", "stone")
configLists("fibre", "grass")
configList("tweaks", "lump_list", "stone, desert_stone")

return config