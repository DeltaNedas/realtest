local overrides = {}

overrides.replaceNode = function(name, def)
	if minetest.registered_nodes[name] then
		minetest.register_node(":"..name, def) -- Override node e.g. default:dirt -> :default:dirt
		return true
	end
end
overrides.overrideNode = function(name, merge)
	if name then
		local old = minetest.registered_nodes[name]
		if old then
			for i, v in pairs(merge or {}) do
				old[i] = v
			end

			return overrides.replaceNode(name, old)
		end
	end
end

overrides.replaceItem = function(name, def)
	if minetest.registered_items[name] then
		minetest.unregister_item(name)
		minetest.register_craftitem(name, def)
		return true
	end
end
overrides.overrideItem = function(name, merge)
	if name then
		if minetest.registered_items[name] then
			return minetest.override_item(name, merge)
		end
	end
end

overrides.replaceRecipe = function(oldInput, recipe)
	minetest.clear_craft{
		type = recipe.type,
		recipe = oldInput
	}
	minetest.register_craft(recipe)
	return true
end
overrides.removeShapeless = function(...)
	return minetest.clear_craft{
		type = "shapeless",
		recipe = {...}
	}
end
overrides.removeShaped = function(...)
	return minetest.clear_craft{
		recipe = {...}
	}
end

-- Actual overriding
local config = realtest.config

minetest.register_on_mods_loaded(function()

if config.logs.enable_chopping then
	-- Remove log/stem -> plank recipes
	overrides.removeShaped{"default:tree"}
	overrides.removeShaped{"default:jungletree"}
	overrides.removeShaped{"default:pine_tree"}
	overrides.removeShaped{"default:acacia_tree"}
	overrides.removeShaped{"default:aspen_tree"}

	overrides.removeShaped{"default:bush_stem"}
	overrides.removeShaped{"default:pine_bush_stem"}
	overrides.removeShaped{"default:acacia_bush_stem"}

	-- Add axe plank "crafting recipe"
	for name, def in pairs(minetest.registered_items) do
		if realtest.isValidAxe(name) then
			local old_on_place = def.on_place or minetest.item_place
			local sound = (def.sounds or {}).dug or default.node_sound_wood_defaults().dug

			overrides.overrideItem(name, {
				on_place = function(itemstack, player, pointed)
					local node = minetest.get_node(pointed.under)
					local sneaking = player:get_player_control().sneak

					if sneaking or not realtest.isValidTree(node) then
						return old_on_place(itemstack, player, pointed)
					end

					local name = node.name
					local tree = name:match("[:_](tree)$") or name:match("[:_](log)$") -- Support minecraft-like names too
					local stem = name:match("[:_](bush_stem)$")

					local using = tree or stem

					local plank = "default:wood "
					if using and name:match(using) then -- Give apple wood by default
						plank = name:gsub(using, "wood").." "
					end

					if stem then
						plank = plank..realtest.config.logs.stem_plank_count
					else
						plank = plank..realtest.config.logs.log_plank_count
					end

					minetest.remove_node(pointed.under)
					minetest.handle_node_drops(pointed.under, {plank}, player)
					minetest.sound_play(sound, {pos = pointed_under})

					if not minetest.settings:get_bool("creative_mode") then
						if itemstack:add_wear(math.floor(2^16 / 25 + 0.5)) then -- Only wear down tools
							return itemstack
						end
					end
				end
			})
		end
	end
	for name, def in pairs(minetest.registered_nodes) do
		if realtest.isValidTree(name) then
			minetest.log(name.." is unbreakable by hand now.")
			overrides.overrideItem(name, {
				groups = realtest.util.modify(def.groups, "oddly_breakable_by_hand", nil) -- Make it so you cant break trees or stems with your hands]
			})
		end

		local leaves = (def.groups or {}).leaves
		local chance = realtest.config.tweaks.leaf_stick_drop_chance

		if chance > 0 and leaves and leaves > 0 then
			local drop = def.drop
			if type(drop) ~= "table" then
				drop = {
					max_items = 1,
					items = {
						{
							items = {drop}
						}
					}
				}
			end

			table.insert(drop.items, #drop.items, {
				items = {"default:stick"},
				rarity = chance
			})

			overrides.overrideNode(name, {
				drop = drop
			})
		end
	end
end

if config.flint.enable_flint_shards then
	local sound = default.node_sound_gravel_defaults().dug
	overrides.overrideItem("default:flint", {
		on_place = function(itemstack, player, pointed)
			if pointed.type == "node" then -- No breaking flint over peoples heads
				local node = minetest.get_node(pointed.under)
				if node and realtest.isValidStone(node) then -- Only break on stone-y blocks
					minetest.handle_node_drops(
						pointed.under,
						{"realtest:flint_shards "..config.flint.flint_shard_count},
						player)
					minetest.sound_play(sound, {pos = pointed_under})

					if not minetest.settings:get_bool("creative_mode") then
						return "default:flint "..(itemstack:get_count() - 1)
					end
				end
			end
		end
	})
end

if config.tweaks.fast_gravel_mining then
	overrides.overrideNode("default:gravel", {
		groups = {crumbly = 3, falling_node = 1}
	})
end
end)

return overrides