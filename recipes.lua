local recipes = {}
recipes.addFuel = function(name, time)
	return minetest.register_craft{
		type = "fuel",
		recipe = "realtest:"..name,
		burntime = time or 1,
	}
end

recipes.addFuel("plant_fibre", 1)
recipes.addFuel("plant_twine", 4) -- 1 second FREE!

local fibre = "realtest:plant_fibre"
local twine = "realtest:plant_twine"
local shards = "realtest:flint_shards"
local flint = "default:flint"
local stick = "default:stick"
local wood = "group:wood"

minetest.register_craft{
	type = "shapeless",
	output = twine.." 2",
	recipe = {fibre, fibre, fibre}
}

minetest.register_craft{
	output = "realtest:flint_pickaxe",
	recipe = {
		{shards, wood, shards},
		{twine, stick, twine},
		{"", stick, ""}
	}
}

minetest.register_craft{
	output = "realtest:flint_knife",
	recipe = {
		{"", "", shards},
		{"", shards, ""},
		{stick, "", ""}
	}
}

minetest.register_craft{
	output = "realtest:flint_axe",
	recipe = {
		{shards, shards, twine},
		{shards, stick, ""},
		{"", stick, ""}
	}
}

minetest.register_craft{
	output = "realtest:flint_shovel",
	recipe = {
		{"", "", shards},
		{"", flint, ""},
		{stick, stick, ""}
	}
}

return recipes