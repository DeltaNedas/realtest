local function addItem(name, image)
	return minetest.register_craftitem("realtest:"..name, {
		description = realtest.i18n.format("item."..name..".name"),
		inventory_image = image or name..".png"
	})
end
addItem("flint_shards")
addItem("plant_fibre")
addItem("plant_twine")

local function addTool(name, capabilities, extras, group)
	local tool = {
		description = realtest.i18n.format("item.flint_"..name..".name"),
		inventory_image = "flint_"..name..".png",
		stack_max = 1,
		range = 3, -- 1 less range as it's flimsy
		tool_capabilities = capabilities,
		sound = {breaks = "default_tool_breaks"},
		groups = {[group or name] = 1}
	}

	for i, v in pairs(extras or {}) do
		tool[i] = v
	end

	return minetest.register_tool("realtest:flint_"..name, tool)
end

local function addLump(name, output)
	local lump = "realtest:"..name.."_lump"
	addItem(name.."_lump", "lump_overlay.png^default_"..name..".png^lump_overlay.png^[makealpha:255,126,126")
	if realtest.config.tweaks.enable_lumps then
		realtest.overrides.overrideNode("default:"..name:gsub("stone$", "cobble"), {
			drop = {
				max_items = 4,
				items = {
					{
						items = {lump.." 4"} -- Cobble drops all 4
					}
				}
			}
		})
		realtest.overrides.overrideNode("default:"..name, {
			drop = {
				max_items = 4,
				items = {
					{
						items = {lump},
						rarity = 2
					},
					{
						items = {lump.." 3"}
					}
				}
			}
		})
		minetest.register_craft{
			output = "default:"..(output or name:gsub("stone$", "cobble")),
			recipe = {
				{lump, lump},
				{lump, lump}
			}
		}
	end
end

for lump, i in pairs(realtest.config.tweaks.lump_list) do
	addLump(lump)
end
addLump("mossycobble", "stone")

addTool("pickaxe", {
	full_punch_interval = 1.3,
	max_drop_level = 0,
	groupcaps = {
		cracky = {times = {[3] = 1.4}, uses = 15, maxlevel = 1} -- Faster than wood, slower than stone
	},
	damage_groups = {fleshy = 3} -- More damage than wood
})

addTool("shovel", {
	full_punch_interval = 1.3,
	max_drop_level = 0,
	groupcaps = {
		crumbly = {times = {[2] = 1.4, [3] = 0.55}, uses = 15, maxlevel = 1} -- Faster than hands :)
	},
	damage_groups = {fleshy = 2} -- Same as wood and stone
})

addTool("axe", {
	full_punch_interval = 1.3,
	max_drop_level = 0,
	groupcaps = {
		choppy = {times = {[2] = 3, [3] = 2}, uses = 15, maxlevel = 1}
	},
	damage_groups = {fleshy = 5}
	-- R-click is managed in overrides.lua
})

local grassSound = default.node_sound_leaves_defaults().dug
addTool("knife", {
	full_punch_interval = 0.9,
	max_drop_level = 0,
	groupcaps = {
		snappy = {times = {[2] = 1.6, [3] = 0.4}, uses = 15, maxlevel = 1}
	},
	damage_groups = {fleshy = 2.5}
}, {
	on_place = function(itemstack, player, pointed)
		if pointed.type == "node" then
			local node = minetest.get_node(pointed.under)
			if realtest.isValidGrass(node) then
				if realtest.config.fibre.plant_fibre_count > 0 and (math.random(1, realtest.config.fibre.plant_fibre_drop_chance) == 1) then
					minetest.remove_node(pointed.under)
					minetest.handle_node_drops( -- Spawn item if no space in inv
						pointed.under,
						{"realtest:plant_fibre "..realtest.config.fibre.plant_fibre_count},
						player)
					minetest.sound_play(grassSound, {pos = pointed.under})
				else
					minetest.dig_node(pointed.under)
				end
				if not minetest.settings:get_bool("creative_mode") then
					if itemstack:add_wear(math.floor(2^16 / 15 + 0.5)) then -- Only wear down tools
						return itemstack
					end
				end
			end
		end
	end
}, "sword")