realtest = {
	util = {},
	i18n = {},
	path = minetest.get_modpath("realtest").."/",
	loadLib = function(lib)
		return dofile(realtest.path..lib..".lua")
	end,
	insertLib = function(lib)
		realtest[lib] = realtest.loadLib(lib) -- Put it in realtest.libname
	end,

	loadLibs = function(...)
		for _, lib in ipairs{...} do
			realtest.loadLib(lib)
		end
	end,
	insertLibs = function(...)
		for _, lib in ipairs{...} do
			realtest.insertLib(lib)
		end
	end
}
realtest.insertLibs("util", "i18n", "config")

local old = os.time()
realtest.i18n.log("message.loading")
realtest.insertLibs("overrides", "items", "recipes")
realtest.i18n.log("message.loaded", os.time() - old)