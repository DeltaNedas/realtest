local Util = realtest.util

local I18n = {}

-- A string or function returing a formatted string is accepted.
-- You can use {format:key;args,seperated,by,commas} to embed a translation.
-- Escape the format: as format\: and you can say it literally if you would ever need to.
-- Standard string.format is used otherwise.
-- I18n.toString() returns a suitable {format:<STRING>}.
I18n.locales = {
	en = {
		["message.loading"] = "Loading {format:misc.mod_name}...",
		["message.loaded"] = "Loaded {format:misc.mod_name} in %ds!",
		["misc.mod_name"] = "RealTest",

		["item.flint_shards.name"] = "Flint Shards\nBreak flint on a rock to get.",
		["item.flint_knife.name"] = "Flint Knife\nRight click grass to cut fibres.",
		["item.flint_axe.name"] = "Flint Hatchet\nRight click trees to chop planks",
		["item.flint_pickaxe.name"] = "Flint Pick",
		["item.flint_shovel.name"] = "Flint Trowel",

		["item.plant_fibre.name"] = "Plant Fibre\nSee Flint Knife.",
		["item.plant_twine.name"] = "Plant Twine",

		["item.stone_lump.name"] = "Stone Lump",
		["item.desert_stone_lump.name"] = "Desert Stone Lump",
		["item.mossycobble_lump.name"] = "Mossy Stone Lump"
	}
}

I18n.locale = minetest.settings:get("language") or "en"
if I18n.locale == "" then I18n.locale = "en" end

function I18n.toString(key, ...) -- Return "key;str,num,etc" or "key"
	if not key then return "" end
	local varArgs = {...}
	key = tostring(key)

	if #varArgs == 0 then
		return key
	end
	return key..";"..table.concat({...}, ",")
end

function I18n.fromString(str)
	assert(type(str) == "string", "bad argument #1, expected string (got "..type(str)..")")
	local key = str
	local formats = {}
	if str:match("^.-;.*") then
		key = str:match("^(.-);")
		formats = Util.split(str:match("^.-;(.*)"), ",")
	end
	return key, unpack(formats)
end

local using = I18n.locales[I18n.locale]
if using then
	function I18n.format(key, ...)
		if not key then return "" end
		local result = using[key]
		if not result then
			minetest.log("warning", "No localisation found for '"..key.."'!")
			return I18n.toString(key, ...)
		end

		local resultType = type(result)
		local res

		if resultType == "string" then
			res = result:format(...)
		elseif resultType == "function" then
			res = result(...)
		end
		if type(res) ~= "string" then
			return I18n.toString(key, ...) -- string or string returning functions only!
		end

		for inline in res:gmatch("{format:(.-)}") do
			res = res:gsub("{format:"..Util.escape(inline).."}", I18n.format(I18n.fromString(inline)))
		end

		return (res:gsub("{format\\:", "{format:"))
	end
else
	I18n.format = I18n.toString
	minetest.log("No locale found for "..I18n.locale.."!")
end


I18n.escapeColours = { -- Minecraft style colours with bukkit (i think) escapes
	{"0", "000000"}, -- Black
	{"1", "0000AA"}, -- Blue
	{"2", "007F00"}, -- Green
	{"3", "00AAAA"}, -- Cyan
	{"4", "7F0000"}, -- Red
	{"5", "7F007F"}, -- Purple
	{"6", "FFA500"}, -- Orange
	{"7", "C0C0C0"}, -- Light Grey
	{"8", "7F7F7F"}, -- Grey
	{"9", "0000FF"}, -- Bright Blue
	{"a", "00FF00"}, -- Bright Green
	{"b", "00FFFF"}, -- Bright Cyan
	{"c", "FF0000"}, -- Bright Red
	{"d", "FF00FF"}, -- Pink
	{"e", "FFFF00"}, -- Bright Yellow
	{"f", "FFFFFF"}, -- White
}

function I18n.formatC(...)
	local text = I18n.format(...)
	for _, colour in ipairs(I18n.escapeColours) do -- Remove all &X's and colour the text
		text = text:gsub("&"..colour[1], minetest.get_color_escape_sequence("#"..colour[2]))
		text = text:gsub("&#"..colour[1], minetest.get_background_escape_sequence("#"..colour[2]))
	end
	return text
end

function I18n.print(...)
	return print(I18n.format(...))
end
function I18n.log(...)
	return minetest.log(I18n.format(...))
end

function I18n.printC(...)
	return print(I18n.formatC(...))
end
function I18n.logC(...)
	return minetest.log(I18n.formatC(...))
end

return I18n