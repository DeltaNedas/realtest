local Util = {}

function Util.switch(value, cases, default)
	if #cases == 0 then return end
	if value == nil then return end

	for case, func in pairs(cases) do
		if value == case then
			return func()
		end
	end
	if default then
		return default()
	end
end

function Util.split(str, sep)
	assert(type(str) == "string", "bad argument #1, expected string (got "..type(str)..")")
	sep = (((type(sep) == "string") and sep) or ",") -- Default is ,

	local ret = {}
	if #str > 0 then
		if sep == "" then
			for i = 1, #str do
				table.insert(ret, str:sub(i, i))
			end
		else
			for matched in string.gmatch(str, "[^"..sep.."]+") do
				table.insert(ret, matched)
			end
		end
	end

	return ret
end

function Util.unpack(t, i)
	i = i or 1
	if t[i] then
		return t[i], Util.unpack(t, i + 1)
	end
end

function Util.escape(str)
	assert(type(str) == "string", "bad argument #1, expected string (got "..type(str)..")")
	return str:gsub("(%W)", "%%%1")
end

function Util.capitalise(str, amount)
	amount = tonumber(amount or 1)
	return str:sub(1, amount):upper()..str:sub(amount + 1)
end

function Util.find(array, member, access)
	if access then -- array[i].access == member
		for i, t in ipairs(array) do
			if t[access] == member then
				return t[access], i
			end
		end
		return false
	end

	for i, found in ipairs(array) do
		if found == member then
			return found, i
		end
	end
	return false
end

local ppii = math.pi * 2
function Util.decToDeg(dec)
	if type(dec) == "table" then
		for i, v in pairs(dec) do
			dec[i] = Util.decToDeg(v)
		end
		return dec
	end
	return dec * 360
end
function Util.decToRad(dec)
	if type(dec) == "table" then
		for i, v in pairs(dec) do
			dec[i] = Util.decToRad(v)
		end
		return dec
	end
	return dec * ppii
end
function Util.degToRad(deg)
	if type(deg) == "table" then
		for i, v in pairs(deg) do
			deg[i] = Util.degToRad(v)
		end
		return deg
	end
	return deg / 180 * math.pi
end

function Util.degToDec(deg)
	if type(deg) == "table" then
		for i, v in pairs(deg) do
			deg[i] = Util.degToDec(v)
		end
		return deg
	end
	return dec / 360
end
function Util.radToDec(rad)
	if type(rad) == "table" then
		for i, v in pairs(rad) do
			rad[i] = Util.radToDec(v)
		end
		return rad
	end
	return rad / ppii
end
function Util.radToDeg(rad)
	if type(rad) == "table" then
		for i, v in pairs(rad) do
			rad[i] = Util.radToDeg(v)
		end
		return rad
	end
	return rad / math.pi * 180
end

function Util.clamp(n, min, max)
	return (n < min and min) or (n > max and max) or n
end

function Util.modify(t, i, v)
	t[i] = v
	return t
end

function Util.copy(old)
	local new = {}
	for i, v in pairs(old) do -- Copy fields
		if type(v) == "table" then
			if v == old then
				new[i] = new
			else
				new[i] = Util.copy(v)
			end
		else
			new[i] = v
		end
	end

	setmetatable(new, getmetatable(old) or {}) --
	return new
end

-- Not in util namespace, practical functions instead of calculation stuff

realtest.isValidAxe = function(itemstack) -- Axe selection
	local name = (type(itemstack) == "string" and itemstack) or itemstack:get_name()

	if realtest.config.logs.axe_blacklist[name] then
		return false
	end
	if realtest.config.logs.axe_whitelist[name] then
		return true
	end

	if realtest.config.logs.enable_auto_axe_whitelist then
		local registered = minetest.registered_items[name]
		if registered then
			local axe = (registered.groups or {}).axe
			return axe and axe > 0
		end
	end
	return false
end
realtest.isValidTree = function(node) -- Axe usage
	local name = (type(node) == "string" and node) or node.name
	if name == "ignore" then
		return false
	end

	if realtest.config.logs.tree_blacklist[name] then
		return false
	end
	if realtest.config.logs.tree_whitelist[name] then
		return true
	end

	if realtest.config.logs.enable_auto_tree_whitelist then
		local registered = minetest.registered_nodes[name]
		if registered then
			local tree = (registered.groups or {}).tree
			return name:match("[:_]bush_stem$") or name:match("[:_]tree$") -- (tree and tree > 0) or
		end
	end
	return false
end

realtest.isValidGrass = function(node) -- Plant Fibre cutting
	local name = (type(node) == "string" and node) or node.name
	if name == "ignore" then
		return false
	end

	if realtest.config.fibre.grass_blacklist[name] then
		return false
	end
	if realtest.config.fibre.grass_whitelist[name] then
		return true
	end

	if realtest.config.fibre.enable_auto_grass_whitelist then
		local registered = minetest.registered_nodes[name]
		if registered then
			local grass = (registered.groups or {}).grass
			return grass and grass > 0
		end
	end
	return false
end

realtest.isValidStone = function(node) -- Flint breaking
	local name = (type(node) == "string" and node) or node.name
	if name == "ignore" then
		return false
	end

	if realtest.config.flint.stone_blacklist[name] then
		return false
	end
	if realtest.config.flint.stone_whitelist[name] then
		return true
	end

	if realtest.config.flint.enable_auto_stone_whitelist then
		local registered = minetest.registered_nodes[name]
		if registered then
			local stone = (registered.groups or {}).stone
			return stone and stone > 0
		end
	end
	return false
end

return Util